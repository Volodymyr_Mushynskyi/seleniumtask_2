package com.epam.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import static com.epam.utils.DriverManager.getDriver;

public class EmailPage {

    private WebDriverWait wait = new WebDriverWait(getDriver(), 10);

    @FindBy(xpath = "//div[@class='T-I J-J5-Ji T-I-KE L3']")
    private WebElement composeButton;

    @FindBy(xpath = "//div[@class='wO nr l1']//textarea")
    private WebElement recipientEmailInputField;

    @FindBy(xpath = "//div[@class='aoD az6']//input")
    private WebElement subjectNameInputField;

    @FindBy(xpath = "//div[@class='Am Al editable LW-avf tS-tW']")
    private WebElement textAreaInputField;

    @FindBy(xpath = "//div[@class='T-I J-J5-Ji aoO v7 T-I-atl L3']")
    private WebElement sendButton;

    @FindBy(xpath = "//div[@class='TN bzz aHS-bnu']//div[@class='qj qr']")
    private WebElement sentEmailsButton;

    @FindBy(name = "volodya2127")
    private WebElement spanRecepient;

    public EmailPage() {
        PageFactory.initElements(getDriver(), this);
    }

    public void clickByComposeButton() {
        wait.until(ExpectedConditions.visibilityOf(composeButton)).click();
    }

    public void clickBySendButton() {
        sendButton.click();
    }

    public void clickBySentEmailsButton() {
        sentEmailsButton.click();
    }

    public void verifyIsMessageSent() {
        wait.until(ExpectedConditions.elementToBeClickable(spanRecepient));
        Assert.assertEquals(spanRecepient.getText(), "volodya2127");
    }

    public void fillRecipientEmailInputField() {
        wait.until(ExpectedConditions.elementToBeClickable(recipientEmailInputField));
        recipientEmailInputField.sendKeys("volodya2127@gmail.com");
    }

    public void fillSubjectNameInputField() {
        subjectNameInputField.sendKeys("USER");
    }

    public void fillTextAreaInputField() {
        textAreaInputField.click();
        textAreaInputField.sendKeys("create new message");
    }
}
