package com.epam.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.epam.utils.DriverManager.getDriver;

public class GmailLoginPage {

    private WebDriverWait wait = new WebDriverWait(getDriver(), 10);

    @FindBy(xpath = "//div[@class='Xb9hP']//input[@type='email']")
    private WebElement emailInputField;

    @FindBy(xpath = "//div[@class='Xb9hP']//input[@type='password']")
    private WebElement passwordInputField;

    @FindBy(id = "identifierNext")
    private WebElement emailNextButton;

    @FindBy(id = "passwordNext")
    private WebElement passwordNextButton;

    public GmailLoginPage() {
        PageFactory.initElements(getDriver(), this);
    }

    public void fillEmailInputField(String email) {
        wait.until(ExpectedConditions.elementToBeClickable(emailInputField));
        emailInputField.sendKeys(email);
    }

    public void fillPasswordInputField(String password) {
        wait.until(ExpectedConditions.elementToBeClickable(passwordInputField));
        passwordInputField.sendKeys(password);
    }

    public void clickByEmailNextButton() {
        emailNextButton.click();
    }

    public void clickByPasswordNextButton() {
        passwordNextButton.click();
    }
}
