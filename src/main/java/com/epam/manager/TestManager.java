package com.epam.manager;

import com.epam.pages.EmailPage;
import com.epam.pages.GmailLoginPage;

public class TestManager {

    public TestManager manageLoginTest() {
        GmailLoginPage gmailLoginPage = new GmailLoginPage();
        gmailLoginPage.fillEmailInputField("jokarp4556@gmail.com");
        gmailLoginPage.clickByEmailNextButton();
        gmailLoginPage.fillPasswordInputField("jjj111???");
        gmailLoginPage.clickByPasswordNextButton();
        return new TestManager();
    }

    public TestManager manageSendingEmailTest() {
        EmailPage emailPage = new EmailPage();
        emailPage.clickByComposeButton();
        emailPage.fillRecipientEmailInputField();
        emailPage.fillSubjectNameInputField();
        emailPage.fillTextAreaInputField();
        emailPage.clickBySendButton();
        emailPage.clickBySentEmailsButton();
        emailPage.verifyIsMessageSent();
        return new TestManager();
    }
}
