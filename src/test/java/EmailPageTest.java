import com.epam.manager.TestManager;
import com.epam.utils.BaseTest;
import org.testng.annotations.Test;

public class EmailPageTest extends BaseTest {

    @Test(description = "send email")
    public void sendingEmail() {
        new TestManager()
                .manageLoginTest()
                .manageSendingEmailTest();
    }
}
